How to buy as a guest on eBay
Once you’ve found an item you’d like to buy, you can go to checkout to complete your purchase. Here's how:


Select Buy It Now on the listing.
Select Check out as guest.
Fill in your shipping address, email address, and payment information.
Select Confirm and pay.
You can buy without an eBay account as long as:


The item costs less than $5,000
The item can be purchased using Buy It Now. If you want to bid on an auction or send a Best Offer to a seller, you’ll need an eBay account
You pay for the item using PayPal, credit card, debit card, Apple Pay, or Google Pay
Your guest order confirmation email
When you buy an item as a guest, we’ll send you a guest order confirmation email, with the subject line “Order Confirmed”. You'll need this email to view your order details, contact the seller, track your order, or start a return if you need to send your item back.


If you can’t find your guest order confirmation email in your inbox, select the button below and we’ll send you a link to your order details.