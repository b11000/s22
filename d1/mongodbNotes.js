// What is database
// A database is an organized collection of information or data.
// Data is different from information. Data is raw and does not carry any specific meaning.
// Information on the other hand is a group of organized data that conatains logical meaning.
// A database is managed by what is called a database management system
// What is database management system
// A database management system (or DBMS) is a system specifically designed to manage the storage, retrieval and modification of data in a database.
// Continuing on the example earlier, the DBMS of a physical library is the Dewey Decimal System.
// Database systems allows four types of operations when it comes to handling data, namely create(insert), read(select),update and delete.
// Collectively it is called the CRUD operations.
// Relational database is a type of database where data is stored as a set of tables with rows and columns(pretty much like a table printed on a piece of paper)
// For unstructured data(data that cannot fit into a strict tabular format), NoSQL databases are commonly used.
// 
// What is SQL?
// SQL stans for structured query language. It is the language used typically in relational DBMS to store, retrieve and modify data.

// A code in SQL looks like this:
// SELECT id, first_name, last_name FROM students WHERE batch number - 1;

//Because of SQL databases require tables and columns to be defined before creation and use, NoSQL databases are becoming more popular due to their flexibility of being able to change the data structure of information in databases on the fly.
//Developer can omit the process of recreating tables and backing up data and re importing them as needed on certain occasions
//
// What is NoSQL?
// No SQL means Not only SQL. NoSQL was conceptualized when capturing comple, unstructure data became more difficult

//Mondo DB is an open-source database and the leading NoSQL database
//its language is highly expressive, and generally friendly to those already familiar with the JSON structure. 

// Mongo in MongoDB is a part of the word humongous which means huge or enormouse

// What does data stored in MondoDB look like

// {
// 	"id" : ObjectID("5d3007ad8a427..."),
// 	"firstName" : "Sylvan",
// 	"lastName" : "Cahilog",
// }
//	field property : field values 
// The main advantage of this data structure is that it is stored in MongoDB using the JSON format.

// Also, in MongoDB, some terms in relational databases will be changed:
// from tables to collections;
// from rows to documents for rows; and
// from columns to fields

// What is data model?
// By creating data models, we anticipate which data will be managed by the database system and the application to be developed.
// To create data model in the session, we will simulate a scenario where an organization wants make a web app to manage the courses they offer.

// Data modelling case simulation
// The web app will start with a simple login procedure.

// Data modelling case simulation
// It is a procedure that most pre-existing apps already have.
// Then, there will be two types of users: the admin of the system and their customers.
// The information about the admin will be placed in the database beforehand. For the customers, they will need to register to the app before logging in.
// During the registration process, they want to get the name and contact details of the customer.
// The process will also ask whether the course is currently being offered or not.
// 
// Data modelling case simulation
// Finally, the customers will enroll toa course and will pay for the courses they've enrolled to creating a transaction.

// The transaction will record the status of the payment and the payment method that the customer used.

// Student Information System
// 
// 
// 		General Use case Diagram
// 				
//    0  --- Manage Student  ---  0
//  <|>						    <|>	
//  /\							/\
//  
//  
//  Creating the data models
//  The users will contain the information of both the customers and the admin.
//  Next, we identify which fields are needed by each data model.
// 
//  Course Booking Application
//  Data Models
//  ======Users======
//  {
//  	_id: ObjectId,
//  	firstName: String,
//  	lastName: String,
//  	age: String,
//  	gender: String,
//  	email: String,
//  	password: String,
//  	mobileNo: String,
//  	isAdmin: Boolean,
//  	enrollments: [
//  		{
//  			courseId: String,
//  			status: String,
//  			enrolledOn: Date
//  		}
//  	],
//  	paymentStatus: transactionId
//  }
//  ======Courses======
//  {
//  	name: String,
//  	description: String,
//  	price: Number,
//  	isActive: Boolean,
//  	createdOn: Date,
//  	enrollees: [
//  		{
//  			userId: String,
//  			enrolledOn: Date,
//  		}
//  	]	
//  }
//  ======Transactions======
//  
//  {
//  	_id: ObjectId,
//  	userId: String,
//  	courseId: String,
//  	isPaid: Boolean,
//  	totalAmount: Number,
//  	paymentMethod: String,
//  	datetimeCreated: Date
//  }
//
// Data Model Design- The key consideration for the structure of your documents.
// ======Embedded Data Model======
//  {
//  	name: "ABC",
//  	contact: "093483843",
//  	branches: 
//  	[
//  		{
//  			_id: "001"
//  			name: "branch1",
//  			address: "QC",
//  			contact: "09485748454"
//  		}
//  	]
//  }
//   
// {
//  	userName: "rhakistaGhirl005",
//  	contact: {
//  		mobileNo: "099343",
//  		phoneNo: "1233-343-53",
//  		email: rhakistaGhirl005@mail.com,
//  		faxNo: "09350348943354"
//  	} 
// } 
// ======Normalized/Reference Data Model======
// branch model
// {
// 		_id: String
// 		name: String,
// 		address: String,
// 		city: String,
// 		supplier_id: <supplier_id>
// }
// suppliers model
// 
// {
// 		_id: String
// 		name: String,
// 		contact:
// 		branch:[
// 			<branch_id>
// 		]
// }
// 
//Now that we have identified the data models, we then identify their relationships
//  Relationships can have three types:
//  One-to-one
//  E.g. A person can only have one employee ID on a given company.
//  One-to-many
// E.g. A person can have one or more email address.
// Many-to-many
// E.g. A book can have multiple Authors
// 
// Activity:
// 1. In the S22 folder, create an activity folder.
// 2. Go to diagrams.net and link your professional gmail account.

// 3. Using the given set of entities and attributes, create a use case diagram for scenario of an Ecommerce application:
// * Users
// - First Name
// - Last Name
// - Email
// - Password
// - Is Admin?
// - Mobile Number
// 
// * Order
// - User Id
// - Transaction Date
// - Status
// - Total

// * Products
// - Name
// - Description
// - Price
// - Stocks
// - Is Active?
// - SKU
// 
// * Order Products
// - Order ID
// - Product ID
// - Quantity
// - Price
// - Sub Total

// 4. Copy the diagram into the activity folder.
// 5. Create a mock data for each of the following collection using JSON syntax. And saved it as modelName.json (json). Per Data Model
// 6. Create a git repository named S22.
// 7. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code. "s22 Activity"
// 8. Add the link in Boodle."S22 - MongoDB - Data Modeling and Translation"
// guys, palagay rin pala ako sa isang .txt file ng lahat ng models niyo then yung data type. katulad ng discussion natin sa booking app eccomerce_datamodel.txt
// 
// {
// 	"name": "jane Dela Cruz"
// }

// sample for the .json

// {
// "_id": "601ccbe89bcd482ee8fa2c99",
// "name": "HTML",
// "description": "Learn the basics and how to code.",
// "price": 1000,
// "isActive": true,
// "slots": "20",
// "enrollees": ["507f1f77bcf86cd799439011"],
// "datetimeCreated": "2020-03-10T15:00:00.00Z"
// }